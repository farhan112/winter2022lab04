public class Food{
	
	private String nameOfFood;
	private double cost;
	private String cuisineType;
	
	//Constructor
	public Food(String nameOfFood, double cost, String cuisineType){
		this.nameOfFood = nameOfFood;
		this.cost = cost;
		this.cuisineType = cuisineType;
	}
	
	//Name of Food
	public String getNameOfFood(){
		return this.nameOfFood;
	}
	
	/*public void setNameOfFood(String nameOfFood){
		this.nameOfFood = nameOfFood;
	}*/
	
	//Cost
	public double getCost(){
		return this.cost;
	}
	
	public void setCost(double cost){
		this.cost = cost;
	}
	
	//Cuisine Type
	public String getCuisineType(){
		return this.cuisineType;
	}
	
	public void setCuisineType(String cuisineType){
		this.cuisineType = cuisineType;
	}
	
	//Instance method
	public void rate(){
		
		System.out.println(this.nameOfFood + " is a 5 star dish for only " + this.cost + "$!");
		
	}
	
}