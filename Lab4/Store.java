import java.util.Scanner;
public class Store{
	
	public static void main(String[] args){
		
		//Initialisation
		Scanner reader = new Scanner(System.in);
		Food[] menu = new Food[4];
		String nameOfFood = "Name of the Food";
		double cost = 0.0;
		String cuisineType = "Type of Cuisine";
		
		//Input
		for(int i = 0; i < 4; i++){
			
			System.out.println("Enter the name of dish #" + (i+1) +".");
			nameOfFood = reader.nextLine();
			
			System.out.println("Enter the cost of dish #" + (i+1) +".");
			cost = Double.parseDouble(reader.nextLine());
			
			System.out.println("Enter the type of cuisine of dish #" + (i+1) +".");
			cuisineType = reader.nextLine();
			
			menu[i] = new Food(nameOfFood, cost, cuisineType);
			
		}
		
		//Output
		System.out.println("The name of dish #4 is: " + menu[3].getNameOfFood());
		System.out.println("The cost of dish #4 is: " + menu[3].getCost() + "$");
		System.out.println("The type of cuisine of dish #4 is: " + menu[3].getCuisineType());
		menu[3].rate();
		
		//Use Setter
		System.out.println("The cost of dish #4 should be different! Change it!");
		menu[3].setCost(Double.parseDouble(reader.nextLine()));
		
		System.out.println("The name of dish #4 is: " + menu[3].getNameOfFood());
		System.out.println("The cost of dish #4 is: " + menu[3].getCost() + "$");
		System.out.println("The type of cuisine of dish #4 is: " + menu[3].getCuisineType());
		menu[3].rate();
	}
	
}